package com.itsnuman.touristguidesylhet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

/**
 * A fragment representing a single Location detail screen.
 * This fragment is either contained in a {@link LocationListActivity}
 * in two-pane mode (on tablets) or a {@link LocationDetailActivity}
 * on handsets.
 */
public class LocationDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private AvailableLocations.Location mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LocationDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            AvailableLocations al = new AvailableLocations(getActivity().getApplicationContext());
            mItem = al.getLocationMap().get(getArguments().getString(ARG_ITEM_ID));


            Activity activity = this.getActivity();

            ImageView background_image = (ImageView) activity.findViewById(R.id.location_background);
            if( null != background_image ) {
                try {
                    background_image.setImageDrawable(Drawable.createFromStream(getActivity().getAssets().open(mItem.imgPath), null));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.name);
            }

            FloatingActionButton mapBtn = (FloatingActionButton) activity.findViewById(R.id.fab);
            if( null != mapBtn ) {
                mapBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity().getApplicationContext(), MapsActivity.class);
                        intent.putExtra("lat", mItem.lat);
                        intent.putExtra("lng", mItem.lng);
                        intent.putExtra("location_name", mItem.name);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.location_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.location_detail)).setText(mItem.description.trim());
            ((TextView) rootView.findViewById(R.id.pro_tips)).setText("");
            for( String tip: mItem.pro_tips){
                ((TextView) rootView.findViewById(R.id.pro_tips)).append("• "+tip+"\n");
            }
        }

        return rootView;
    }
}
