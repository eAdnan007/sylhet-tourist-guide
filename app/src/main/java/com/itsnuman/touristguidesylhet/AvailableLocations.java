package com.itsnuman.touristguidesylhet;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AvailableLocations {

    /**
     * An array of locations.
     */
    private final Context context;
    private List<Location> ITEMS = new ArrayList<>();

    /**
     * A map of sample locations, by ID.
     */
    public Map<String, Location> ITEM_MAP = new HashMap<>();

    private static final String location_path = "locations";


    public AvailableLocations(Context c) {
        context = c;

        try {
            String [] locations = context.getAssets().list(location_path);

            for( String location_path: locations ){
                addItem(createLocation(location_path));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Location> getLocationsList() {
        Log.d("AllLocations", ITEMS.toString());
        return ITEMS;
    }

    public Map<String, Location> getLocationMap() {
        return ITEM_MAP;
    }

    private void addItem(Location item) {
        Log.d("Add location", item.slug);
        ITEMS.add(item);
        ITEM_MAP.put(item.slug, item);
    }

    private Location createLocation(String location_path) {
        return new Location(location_path);
    }

    /**
     * A location representing it's details.
     *
     */
    public class Location {
        public final String slug;
        public String name;
        public String description;
        public String imgPath;
        public float lat;
        public float lng;
        public ArrayList<String> pro_tips = new ArrayList<>();


        public Location(String id) {
            this.slug = id;

            parseLocationData();
        }

        private void parseLocationData() {
            try {
                InputStream is = context.getResources().getAssets().open("locations/"+slug+"/data.xml");

                XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
                XmlPullParser myParser = xmlFactoryObject.newPullParser();

                myParser.setInput(is, null);
                int event = myParser.getEventType();
                String text = "";
                boolean tips_started = false;
                while (event != XmlPullParser.END_DOCUMENT)
                {
                    String name=myParser.getName();
                    switch (event){
                        case XmlPullParser.START_TAG:
                            if(name.equals("pro-tips"))
                                tips_started = true;
                            break;
                        case XmlPullParser.TEXT:
                            text = myParser.getText();
                            break;
                        case XmlPullParser.END_TAG:
                            switch (name) {
                                case "coordinates":
                                    this.lat = Float.parseFloat(myParser.getAttributeValue(null, "lat"));
                                    this.lng = Float.parseFloat(myParser.getAttributeValue(null, "lng"));
                                    break;
                                case "name":
                                    this.name = text;
                                    break;
                                case "description":
                                    this.description = text;
                                    break;
                                case "featured-image":
                                    this.imgPath = location_path + "/" + slug + "/" + myParser.getAttributeValue(null, "src");
                                    break;
                                case "pro-tips":
                                    tips_started = false;
                                    break;
                                case "tip":
                                    if(tips_started)
                                        pro_tips.add(text);
                                    break;
                            }
                            break;
                    }
                    event = myParser.next();
                }

            } catch (IOException | XmlPullParserException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return slug;
        }
    }
}
